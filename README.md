# Identificar sites que não estejam conformes às políticas legais

Uma aplicação para analisar o conteúdo de \textit{websites} e verificar se o produto/serviço oferecido é ou não é proibido para ser exibido pela empresa de pagamentos eletrônicos.

## O Ambiente

O projeto é desenvolvido principalmente em python. A parte de back-end, que inclui a criação do servidor que recebe requisições POST, assim como a verificação dos sites e o envio dos resultados.

Para a parte do front-end são utilizadas apenas tecnologias web, isto é HTML, CSS e Javascript.

### Back-end
Para o servidor saõ necessários:

* python 3
* mongodb

Além disso é preciso ter instaladas algumas bibliotecas para python. Primeiro instalamos a biblioteca de mongodb para python:

```
$ pip install pymongo
```

Outra biblioteca necessária é a de tradução de google para python:

```
$ pip install git+https://github.com/BoseCorp/py-googletrans.git --upgrade
```

Para os classificadores são necessárias algumas bibliotecas de processamento de linguagem natural

```
$ pip install rake-nltk
$ pip install -U gensim
$ python -c "import nltk; nltk.download('stopwords')"
$ python -c "import nltk; nltk.download('wordnet')"
```

### Front-end

Para enviar as requisições POST ao servidor foi usada a ferramenta curl

```
$ curl http://localhost:3000 -d "@urls.json" -H "content-type: application/json"
```

## Usar o projeto

Para executar o servidor precisamos ir no diretório src/server e executar o seguinte comando

```
$ python server.py
```

Logo podemos fazer as requisições POST como especificado na parte do cliente, fazendo uso da ferramento curl.
