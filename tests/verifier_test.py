import sys
sys.path.insert(0, '../src/server')

import verifier

from requisition import Requisition
from website import WebSite

from bs4 import BeautifulSoup
from googletrans import Translator

import datetime

import unittest
from unittest import mock

class VerifierTestCase(unittest.TestCase):
	
	@mock.patch('verifier.requests')
	def test_obtain_html_URL_calls(self, mock_requests, autospec=True):
		# set up the mock
		verifier.obtain_html_URL("https://www.google.com")
		
		# test that obtain_html_URL called request requests.get with the right parameters
		mock_requests.get.assert_called_once_with("https://www.google.com", headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}, timeout=10, verify=False)

	
	@mock.patch('verifier.BeautifulSoup.findAll')
	def test_obtain_text_URL_calls(self, mock_findAll, autospec=True):
		# set up the mock
		
		verifier.obtain_text_URL("<head><title>The Dormouse's story</title></head>")

		# test that obtain_text_URL called BeautifulSoup.findAll with the right parameters
		mock_findAll.assert_called_once_with(text=True)

	
	def test_obtain_text_URL(self, autospec=True):
		html_doc = """
		<html><head><title>The Dormouse's story</title></head>
		<body>
		<p class="title"><b>The Dormouse's story</b></p>
		"""

		result = """ The Dormouse's story   The Dormouse's story """

		# test for empty html
		self.assertEqual(verifier.obtain_text_URL(""), "")
		# test for the html_doc example
		self.assertEqual(verifier.obtain_text_URL(html_doc), result)


	@mock.patch('verifier.BeautifulSoup.findAll')
	def test_obtain_text_URL_chunks_calls(self, mock_findAll, autospec=True):
		# set up the mock
		
		verifier.obtain_text_URL_chunks("<head><title>The Dormouse's story</title></head>")

		# test that obtain_text_URL called BeautifulSoup.findAll with the right parameters
		mock_findAll.assert_called_once_with('meta')


	def test_obtain_text_URL_chunks(self, autospec=True):
		html_doc = """
			<html>
			<head>
			  <meta charset="UTF-8">
			  <meta name="description" content="Free Web tutorials">
			  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
			  <meta name="author" content="John Doe">
			</head>
			<body>

			<p>All meta information goes before the body.</p>

			</body>
			</html>
		"""

		result = "Free Web tutorials. HTML,CSS,XML,JavaScript"

		# test for empty html
		self.assertEqual(verifier.obtain_text_URL_chunks(""), ". ")
		# test for html_doc example
		self.assertEqual(verifier.obtain_text_URL_chunks(html_doc), result)


	def test_translate_google(self, autospec=True):
		text = "Olá mundo"

		result = "Hello World"

		# test for empty html
		self.assertEqual(verifier.translate_google(""), "")
		# test for html_doc example
		self.assertEqual(verifier.translate_google(text, from_language="pt", to_language="en"), result)


	@mock.patch("verifier.Translator.translate")
	def test_translate_API(self, mock_translate, autospec=True):
		# set up the mock
		translator = verifier.Translator()
		text = "Olá mundo"		

		verifier.translate_API(translator, text, from_language="pt", to_language="en")

		# test for html_doc example
		mock_translate.assert_called_once_with(text, dest='en', src='pt')

	
	def test_verifyURL(self, autospec=True):
		# set up the mock
		requisition_n = Requisition("https://www.callback.com", datetime.datetime.now())
		website_n = WebSite(requisition_n.get_number(), "https://www.ime.usp.br", requisition_n.get_date_arrival())

		website_x, state, restrict, reasons = verifier.verifyURL(website_n)
		
		# test that test_verifyURL gets the right result
		self.assertEqual(state, "processado")
		self.assertIn(restrict, [True, False])


if __name__ == '__main__':
    unittest.main()