import sys
sys.path.insert(0, '../src/server')

from keyword_classifier import KeywordExtractor
from topic_classifier import TopicExtractor

import unittest
from unittest import mock


class KeywordClassifierTest(unittest.TestCase):
	"""testing for KeywordClassifierTest"""

	def test_classify(self, autospec=True):
		# set up the mock
		
		# test KeywordExtractor classification PROHIBITED site
		
		#text = 'Buy Electronic Cigarette, liquid and general accessories for electronic cigarette, various models and brands prompt delivery, come and check prices and delivery times for Brazil .. cigarette-electronic cigarette, e-buy, buy electronic cigarette, , cigarette-electronic-where to buy, cigarette-electronic-in-Brazil, where to buy-cigarette-electronic, ecigs, e-cigarrete, cigarette-electronic-are-paulo.cigarro-electronic-liquid, cigarette-electronic sales , sale-of-cigarette-electronic, tobacco, cigarette, electronic, cigarette-electronic-tobacco-sp.'
		text = 'How to use Cytotec where to buy, lowest price, true x false. step by step. How many tablets to take according to the time of pregnancy.'

		model = KeywordExtractor()
		restrict, reasons = model.classify(text)
		
		self.assertTrue(restrict)
		self.assertNotEqual(len(reasons), 0)

		# test KeywordExtractor classification NOT PROHIBITED site
		text = "Itambé Alimentos S / A is a mining company considered one of the largest dairy companies in the country. To speak of Itambé is to talk about family, work, growth, simplicity."

		restrict, reasons = model.classify(text)
		
		self.assertFalse(restrict)
		self.assertEqual(len(reasons), 0)


class TopicClassifierTest(unittest.TestCase):
	"""testing for TopicClassifierTest"""

	def test_classify(self, autospec=True):
		# set up the mock
		
		# test TopicExtractor classification NOT PROHIBITED site
		
		#text = 'Buy Electronic Cigarette, liquid and general accessories for electronic cigarette, various models and brands prompt delivery, come and check prices and delivery times for Brazil .. cigarette-electronic cigarette, e-buy, buy electronic cigarette, , cigarette-electronic-where to buy, cigarette-electronic-in-Brazil, where to buy-cigarette-electronic, ecigs, e-cigarrete, cigarette-electronic-are-paulo.cigarro-electronic-liquid, cigarette-electronic sales , sale-of-cigarette-electronic, tobacco, cigarette, electronic, cigarette-electronic-tobacco-sp.'
		text = 'How to use Cytotec where to buy, lowest price, true x false. step by step. How many tablets to take according to the time of pregnancy.'

		model = TopicExtractor()
		restrict, reasons = model.classify(text)
		
		self.assertTrue(restrict)
		self.assertNotEqual(len(reasons), 0)

		# test KeywordExtractor classification NOT PROHIBITED site
		text = "Itambé Alimentos S / A is a mining company considered one of the largest dairy companies in the country. To speak of Itambé is to talk about family, work, growth, simplicity."

		restrict, reasons = model.classify(text)
		
		self.assertFalse(restrict)
		self.assertEqual(len(reasons), 0)

if __name__ == '__main__':
    unittest.main()