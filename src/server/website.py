from pymongo import MongoClient

import datetime

class WebSite():

	def __init__(self, requisition, url, date_get):
		self._number = 0
		self._requisition = requisition
		self._url = url		
		self._date_get = date_get
		self._date_verified = None
		self._state = 'aguardando'				#[aguardando, processando, processado, respondido]
		self._restrict = False
		self._reasons = None

	def save(self, websites):
		try:
			new_website = 	{
								"requisition": self._requisition,
								"url": self._url,
								"date_get": self._date_get,
								"date_verified": self._date_verified,
								"state": self._state,
								"restrict": self._restrict,
								"reasons": self._reasons
							}
			self._number = websites.insert_one(new_website).inserted_id			
		except Exception as e:
			raise e

	def update_verification(self, websites, state, restrict = False, reasons = []):
		'''
			Updates the state of verification for the website
		'''
		try:
			self._state = state
			self._restrict = restrict
			self._reasons = reasons

			query_id = { "_id": self._number }
			newvalues = None

			if state == 'processado':
				self._date_verified = datetime.datetime.now()
				newvalues = { "$set": { "date_verified": self._date_verified, "state": state, "restrict": restrict, "reasons": reasons } }
			else:
				newvalues = { "$set": { "state": state } }		

			websites.update_one(query_id, newvalues)
		except Exception as e:
			raise e
		

	def get_url(self):
		return self._url

	def verification_time(self):
		'''
			Returns the duration of time that the site took to be verified
		'''
		return self._date_verified - self._date_get