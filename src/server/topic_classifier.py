from nltk.corpus import stopwords 
from nltk.stem.wordnet import WordNetLemmatizer

import gensim
from gensim import corpora

import string

from check_string import StringComparer

class TopicExtractor():
	
	def classify(self, text):
		'''
			Latent Dirichlet Allocation
		'''

		doc_complete = [text]

		stop = set(stopwords.words('english'))
		exclude = set(string.punctuation) 
		lemma = WordNetLemmatizer()

		def clean(doc):
			stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
			punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
			normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
			return normalized

		doc_clean = [clean(doc).split() for doc in doc_complete]

		# Creating the term dictionary of our courpus, where every unique term is assigned an index.
		dictionary = corpora.Dictionary(doc_clean)
		# Converting list of documents (corpus) into Document Term Matrix using dictionary prepared above.
		doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean]

		# Creating the object for LDA model using gensim library
		Lda = gensim.models.ldamodel.LdaModel

		# Running and Trainign LDA model on the document term matrix.
		ldamodel = Lda(doc_term_matrix, num_topics=4, id2word = dictionary, passes=50)

		result_topics = ldamodel.print_topics(num_topics=4, num_words=3)

		# compare obtained topics
		string_comparer = StringComparer()

		restrict = True

		reasons = string_comparer.keyword_matches_phrase(result_topics)

		if len(reasons) < 2:
			restrict = False

		# print("Resultados")
		# print(restrict)
		# print(list(reasons))

		return [restrict, reasons]