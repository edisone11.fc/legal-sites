from pymongo import MongoClient

import datetime

class Requisition():

	def __init__(self, callback, date_arrival):
		self._number = 0
		self._callback = callback
		self._date_arrival = date_arrival		

	def save(self, requisitions):
		try:
			new_requisition = 	{
									"callback": self._callback,
									"date_arrival": self._date_arrival									
								}
			self._number = requisitions.insert_one(new_requisition).inserted_id
			print(self._number)
		except Exception as e:
			raise e

	def get_number(self):
		return self._number

	def get_date_arrival(self):
		return self._date_arrival

	def max_data_verified(self, websites):
		'''
			Returns the datetime that it takes to verify the last url in the requistion
		'''
		dates_verified = websites.find({ "requisition": self._number }, { 'date_verified': 1 }).sort("date_verified", -1)
		return date_verified[0]

	def verification_time(self, websites):
		'''
			Returns the duration of time that it takes to verify all urls in the requistion
		'''
		return max_data_verified() - self._date_arrival

	def urls_requisition(self, websites):
		'''
			Returns the urls that are part of the requisition
		'''
		return websites.find({ "requisition": self._number })