from requisition import Requisition
from website import WebSite

from keyword_classifier import KeywordExtractor
from topic_classifier import TopicExtractor

from pymongo import MongoClient
from bson.json_util import dumps

from googletrans import Translator

from urllib.request import urlopen
from urllib.request import Request
from urllib.parse import urlencode
from urllib.parse import quote

from bs4 import BeautifulSoup

import multiprocessing as mp

import requests
import datetime
import html.parser
import json
import sys
import re

agent = { 'User-Agent': 'test' }

agent2 = {
	'User-Agent':
				"Mozilla/4.0 (\
						compatible;\
						MSIE 6.0;\
						Windows NT 5.1;\
						SV1;\
						.NET CLR 1.1.4322;\
						.NET CLR 2.0.50727;\
						.NET CLR 3.0.04506.30\
					)"
}

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

# FUNCTIONS RELATED TO THE VERIFICATION OF A WEBSITE

def verifyURL(website):
	'''
		verify website
	'''
	try:
		
		# get and process content by the url
		url = website.get_url()

		html_code = obtain_html_URL(url)
			
		text_url = obtain_text_URL_chunks(html_code)

		translator = Translator()
		english_text = translate_google(text_url, 'pt', 'en')

		print(url)
		print([text_url])
		print([english_text])

			
		'''
			classification part
		'''
		# i) automatic keyword extraction
		#model = KeywordExtractor()

		# ii) topic modeling
		model = TopicExtractor()

		[restrict, reasons] = model.classify(english_text)		

		# update status page to processed
		state = 'processado'

		return [website, state, restrict, reasons]

	except Exception as e:
		state = "error"
		return [website, state]

def obtain_html_URL(url, max_timeout = 10):
	'''
		Returns the html code of the web site identified by url
	'''
	try:
		page = requests.get(url, headers=headers, verify=False, timeout=max_timeout)		#to extract page from website
		
		'''
		print(url)
		print('Status {0}', page.status_code)
		print('headers {0}', page.headers)
		'''
			
		html_code = page.content        		#to extract html code from page
		return html_code
	except Exception as e:
		print(e)
		return ''

def obtain_text_URL(html_code):
	'''
		Returns the text inside the html code of a web site identified by url 
		(Warning: it doesn't throw out javascript code)
	'''
	try:
		soup = BeautifulSoup(html_code, 'html.parser')  		#parse html code
		texts = soup.findAll(text=True)                 		#find all text
		text_from_html = u" ".join(t.strip() for t in texts)    #join all text

		'''
		print('METAS')
		metas = soup.findAll('meta')
		print([ meta.attrs['content'] for meta in metas if 'name' in meta.attrs and meta.attrs['name'] == 'description' ])
		print([ meta.attrs['content'] for meta in metas if 'name' in meta.attrs and meta.attrs['name'] == 'keywords' ])

		'''
		'''
		print('TAGS')
		desc = soup.findAll(attrs={"name": re.compile(r"description", re.I)}) 
		print(desc[0]['content'].encode('utf-8'))
		'''

		return text_from_html
	except Exception as e:
		raise e
		return ''

def obtain_text_URL_chunks(html_code):
	'''
		Returns the text inside a web site identified by url (returns the META DESCRIPTION and META KEYWORDS)
	'''
	try:
		soup = BeautifulSoup(html_code, 'html.parser')  		#parse html code

		metas = soup.findAll('meta')
		description_text = '. '.join([ meta.attrs['content'].strip() for meta in metas if 'name' in meta.attrs and meta.attrs['name'] == 'description' ])
		keywords_text = ', '.join([ meta.attrs['content'].strip() for meta in metas if 'name' in meta.attrs and meta.attrs['name'] == 'keywords' ])
		#print(description_text)
		#print(keywords_text)

		# kill all script and style elements
		for script in soup(["script", "style"]):
		    script.decompose()    # rip it out

		# get text
		text = soup.get_text()

		# break into lines and remove leading and trailing space on each
		lines = (line.strip() for line in text.splitlines())
		# break multi-headlines into a line each
		chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
		# drop blank lines
		text = '. '.join(chunk for chunk in chunks if chunk)


		return description_text + '. ' + keywords_text #+ '. ' +  text
	except Exception as e:
		raise e
		return ''

def translate_google(to_translate, from_language="auto", to_language="auto"):
	'''
		Returns the translation of 'to_translate' using google translate    
	'''
	base_link = "http://translate.google.com/m?hl=%s&sl=%s&q=%s"
		  
	to_translate = quote(to_translate)    
	link = base_link % (to_language, from_language, to_translate)
	request = Request(link, headers=agent)
	raw_data = urlopen(request).read()    
	data = raw_data.decode('ISO-8859-1') #raw_data.decode("utf-8")
	expr = r'class="t0">(.*?)<'
	re_result = re.findall(expr, data)    
	if (len(re_result) == 0):
		result = ""
	else:
		result = html.unescape(re_result[0])
	return result

def translate_API(translator, to_translate, from_language="auto", to_language="auto"):
	'''
		Returns the translation of 'to_translate' using Google Translate Ajax API
		(Warning: the limit of characters is 5000)
	'''
	try:
		return translator.translate(to_translate, src=from_language, dest=to_language).text
	except Exception as e:
		print(e)
		return ''

class URLverifier():

	def __init__(self, urls, callback):
		self._urls = urls
		self._callback = callback
		self._requisition_n = None
		self._requisition_websites = []

		# database connection
		client = MongoClient('localhost', 27017)
		database = client.db_legal_sites
		self._requisitions = database.requisitions
		self._websites = database.websites

	def processURLs(self):
		'''
			Verify a requisition
		'''

		#store requisition
		self._requisition_n = Requisition(self._callback, datetime.datetime.now())
		self._requisition_n.save(self._requisitions)

		# store the urls
		self._requisition_websites = []
		for url in self._urls:
			website_n = WebSite(self._requisition_n.get_number(), url, self._requisition_n.get_date_arrival())
			website_n.save(self._websites)
			self._requisition_websites.append(website_n)
			# update status page to processing
			website_n.update_verification(self._websites, 'processando')

		# Verification part
		
		# parallel computing
		number_processes = 4 # number of processes for parallel computing
		pool = mp.Pool(processes = number_processes)
		
		results = [pool.apply(verifyURL, args=(website_n,)) for website_n in self._requisition_websites]
		#results = [pool.map(verifyURL, self._requisition_websites)]
		
		for result in results:
			if result is None: continue
			website_n, state, restrict, reasons = result
			website_n.update_verification(self._websites, state, restrict, reasons)			

		pool.close()
		pool.join()
		
		#print(results)

		#'''
		# serial computing
		#for website_n in self._requisition_websites:
			#self.verifyURL(website_n)			
		#'''


	def update_sent_results(self):
		'''
			Update status of websites to sent
		'''
		for website_n in self._requisition_websites:
			website_n.update_verification(self._websites, 'enviado')

	def processed_URLs(self):		
		'''
			Return a JSON with the results of the requisition
		'''
		data = self._websites.find({ "requisition": self._requisition_n.get_number() }, { '_id': 0, 'url': 1, 'restrict': 1, 'reasons': 1 })
		return dumps(list(data))
