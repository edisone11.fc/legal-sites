from verifier import URLverifier

from http.server import BaseHTTPRequestHandler, HTTPServer

import json
import cgi
import requests

class GP(BaseHTTPRequestHandler):
    
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
    
    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
        
        # refuse to receive non-json content        
        if ctype != 'application/json':
            self.send_response(400)
            self.end_headers()
            return
            
        # read the message and convert it into a python dictionary
        length = int(self.headers.get('content-length'))
        message = json.loads(self.rfile.read(length))

        #verification
        urls = message['sites']
        callback = message['callback']
        
        # send the message back
        message_to_client = 'The result will be send to: ' + callback
        self._set_headers()
        self.wfile.write(json.dumps(message_to_client).encode())

        #process the urls
        verifier = URLverifier(urls, callback)
        verifier.processURLs()

        #send the results via a POST request
        try:
            results = { 'sites': verifier.processed_URLs() }
            print("FINAL JSON:")
            print(results)
            r = requests.post(callback, data = results)

            verifier.update_sent_results()
            print('RESULTADOS ENVIADOS')
        except Exception as e:
            print("UNABLE TO SEND THE RESULTS TO " + callback)
            raise e

def run(server_class=HTTPServer, handler_class=GP, port=3000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print('Server running at localhost:3000')
    httpd.serve_forever()

run()