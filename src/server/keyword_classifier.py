from rake_nltk import Rake

from check_string import StringComparer

class KeywordExtractor():
	
	def classify(self, text):
		'''
			Algorithm from paper 'Automatic keyword extraction from individual documents' by Stuart Rose, Dave Engel, Nick Cramer and Wendy Cowley
			(https://github.com/csurfer/rake-nltk)
		'''

		# Uses stopwords for english from NLTK, and all puntuation characters by default
		r = Rake()

		# Extraction given the text.
		r.extract_keywords_from_text(text)

		# To get keyword phrases ranked highest to lowest.
		#r.get_ranked_phrases()

		# To get keyword phrases ranked highest to lowest with scores.
		phrases_scores = r.get_ranked_phrases_with_scores()
		#print(phrases_scores)

		# Dictionary of the format [word -> frequency]
		word_frequencies = r.get_word_frequency_distribution()
		#print(word_frequencies)

		# Dictionary of the format [word -> degree]
		word_degrees = r.get_word_degrees()
		#print(word_degrees)


		restrict = True
		
		string_comparer = StringComparer()

		reasons_word_frequencies = string_comparer.keyword_matches_word(word_frequencies)
		reasons_word_degrees = string_comparer.keyword_matches_word(word_degrees)
		reasons_phrases_scores = string_comparer.keyword_matches_phrase(phrases_scores)

		if(not reasons_word_frequencies and not reasons_word_degrees and not reasons_phrases_scores):
			restrict = False

		reasons = set(reasons_word_frequencies) | set(reasons_word_degrees) | set(reasons_phrases_scores)

		# print("Resultados")
		# print(restrict)
		# print(list(reasons))

		return [restrict, list(reasons)]