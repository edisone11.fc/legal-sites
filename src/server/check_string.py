class StringComparer():

	def __init__(self):
		self._restrict_words = [
			"buy","delivery", "price",
			"cigarette", "vapor", "vape", "smoke", "electronic",
			"gun","weapon", "pistol", "hunting", "firearm", "cybergun", "rifle", 
			"girl", "boy", "program", "escort", "scout", "transsexual", "trans", "hot", "prostitution", 
			"tablet", "take", "pregnancy", "drug"
			#"" "resellers"
		]

	def keyword_matches_word(self, list_words):
		matches = []
		for key in list_words:			
			for restrict_word in self._restrict_words:
				if restrict_word in key:
					matches.append(restrict_word)
					break

		return matches

	def keyword_matches_phrase(self, list_phrases):
		matches = []
		for _, phrase in list_phrases:
			for restrict_word in self._restrict_words:
				if restrict_word in phrase:
					matches.append(phrase)
					break

		return matches
